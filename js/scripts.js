d3.queue()
   .defer(d3.json, 'data/links.json')
   .defer(d3.json, 'data/nodes.json')
   .defer(d3.json, 'data/metadata.json')
   .awaitAll(build);

function build(error, files) {
	if (error) throw error;
	berkman("#berkman-demo",files[0],files[1],files[2]);
}