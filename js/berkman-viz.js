function berkman(sel, lData, nData, rData){
  var width = 1000, height = 500, radius = {h:6, w: 27}, space = 4,
      nodeSource = "field_521", nodeTitle = "field_520",
      linkSource = "field_623", linkTarget = "field_624",
      linkOwnership = "field_435",
      container = d3.select(sel),
      hierarchy = {},
      nodeIDs = {},
      disconnected = [],
      centralID = 0,
      falls = 0,
      baseYTransform = 0,
      middle = {x : (width / 2) - radius.w, y : 0},
      stats = container.append("div").attr("id", "stats")
                .attr("class", "stats"),
      svgCont = container.append("div").attr("class", "img"),
      svg = svgCont.append("svg")
              .attr("viewBox", "0 0 "+width+" "+height)
              .attr("width", "100%")
              .attr("height", "100%"),
      reset = container.append("div").attr("id", "reset")
                .attr("class", "reset"),
      elm = svg.append("g").attr("id", "elements");

  var zoom = d3.zoom()
      .scaleExtent([1, 10])
      .translateExtent([[-100, -100], [width + 90, height + 100]])
      .on("zoom", zoomed);

  hierarchy.upper = [];
  hierarchy.lower = [];


  init(lData, nData, rData);

  function init(lData, nData, rData) {

    var edges = new Graph();
    var graph = {};
    graph.links = lData.records;
    graph.nodes = nData.records;
    graph.metadata = rData;

    if(graph.nodes.length !== nData.total_records){
      console.log("Nodes don't equal records");
    }

    graph.nodes.sort(function(a, b) {
        return parseFloat(a[nodeSource]) - parseFloat(b[nodeSource]);
    });

    for (var i = graph.nodes.length - 1; i >= 0; i--) {
      var node = graph.nodes[i];
      node.fixed = true;
      node.children = [];
      node.parents = [];
      nodeIDs[node[nodeSource]] = i;
      if(node.id === graph.metadata.central){
        node.x = height / 2;
        node.y = width / 2;
        node.direction = 0;
        centralID = node[nodeSource];
      }
    };

    graph.links = loadedSort(graph.links);

    var fallthrough = [];
    for (var i = 0; i < graph.links.length; i++) {
      graph.links[i].source = graph.links[i][linkSource];
      graph.links[i].target = graph.links[i][linkTarget];
      evaluateDirection(graph.links[i].source, graph.links[i].target);
      //ensure the node in the link is actually loaded before adding it to our graph
      if((graph.links[i].source in nodeIDs) && (graph.links[i].target in nodeIDs)){
        graph.nodes[nodeIDs[graph.links[i].source]].children.push({id:graph.links[i].target,percent:graph.links[i][linkOwnership]});
        graph.nodes[nodeIDs[graph.links[i].target]].parents.push({id:graph.links[i].source,percent:graph.links[i][linkOwnership]});
        //console.log(i,graph.links[i][linkOwnership]);
        edges.addEdge(graph.links[i].source, graph.links[i].target); 
      }

    };

    // this is an unfortunate function based on state control
    // ideally state issue being caused by evaluate direction would be solved and 
    // this would be removed for efficiency
    evaluateFallthrough();

    function buildViz(){
      //initialize node and link groups in correct order
      var linkgroup = elm.append("g").attr("class", "links").selectAll("polyline");
      var nodegroup = elm.append("g").attr("class", "nodes").selectAll("rect");       
      var labelgroup = elm.append("g").attr("class", "labels").selectAll("text");
      
      var node = nodegroup.data(graph.nodes)
          .enter().append("rect")
          .attr("width", radius.w*2)
          .attr("height", radius.h*2)
          .attr("id", function(d,i){
            return "node"+d[nodeSource];
          })
          .attr("class", function(d,i){
            var cls = "node";
            if(typeof d.direction == "undefined"){
              disconnected.push(d[nodeSource]);
              cls += " undefined";
            }
            d.direction > 0 ? cls += " above" : d.direction == 0 ? cls += " central" : cls += " below";
            return cls;
          })
          .attr("x", middle.y)
          .attr("rx", 3)
          .attr("ry", 3);

      var link = linkgroup.data(graph.links)
        .enter().append("polyline")
        .attr("id", function(d,i){
          return "from" + d[linkSource] + "to" + d[linkTarget];
        })
        .attr("class", function(d,i){
          var cls = "link"
          if(disconnected.indexOf(d[linkSource]) > -1 && disconnected.indexOf(d[linkTarget]) > -1){
            cls += disconnected;
          }
          return cls;
        });

      var labels = labelgroup.data(graph.nodes)
        .enter().append("text")
          .attr("id", function(d,i){
            return "label"+d[nodeSource];
          })
          .attr("dy", "20px")
          .attr("class", function(d,i){
            var cls = "label";
            if(typeof d.direction == "undefined"){
              disconnected.push(d[nodeSource]);
              cls += " undefined";
            }
            d.direction > 0 ? cls += " above" : d.direction == 0 ? cls += " central" : cls += " below";
            return cls;
          })
          .attr("text-anchor", "middle");            

      var currents = elm.append("g").attr("id", "cur");
      currents.append("g").attr("class", "links");
      currents.append("g").attr("class", "nodes");
      currents.append("g").attr("class", "labels");

      node.append("title")
          .text(function(d) { return d[nodeTitle]; });

      bfs(edges, centralID);

      for (var i = graph.nodes.length - 1; i >= 0; i--) {
        if(graph.nodes[i].children.length > 0){
          for (var j = graph.nodes[i].children.length - 1; j >= 0; j--) {
            var n = d3.select("#node"+graph.nodes[i].children[j].id).datum().bfs;
            var dir = d3.select("#node"+graph.nodes[i].children[j].id).datum().direction;
            graph.nodes[i].children[j].shortest = parseInt(n) * dir;
          };

          graph.nodes[i].children.sort(function(a, b){return b-a});
        }
      };

      var disconlinks = d3.select(".links").selectAll("polyline.undefined");
      disconlinks.remove();
      var discon = d3.select(".nodes").selectAll("rect:not(.connected), rect.undefined");
      discon.remove();
      d3.select(".labels").selectAll("text:not(.connected)").remove();

      node.attr("class", function(d,i){
          var cls = d3.select(this).attr("class");
          if(d.children.length > 0){
            d.children.sort(sortChildDe);
            cls += " hasChild oldestChild"+d.children[0].shortest;
          }
          return cls;
        })
        .on("click", function(d,i){
          resetSelect();
          activeNode(d);
          if(d.parents.length > 0){
            parentDisplay(d);
          }
          if(d.children.length > 0){
            childrenDisplay(d);
          }
        });

      reset.on("click", resetSelect);

      var connected = d3.select(".nodes").selectAll(".connected");
      var hasChild = d3.select(".nodes").selectAll(".hasChild:not(.central)")
          .attr("y", function(d,i){
            var cy = hasChildY(this,d);
            return cy;
          });

      node
        .attr("x", function(d,i){
          //add the node to the hierarchy count
          var level = 'upper';
          var l = d.level;
          if(d.direction < 0){
            level = 'lower';
          }
          //console.log(d[nodeTitle], l, level);
          var x = setHierarchy(l,level);
          var cx = circleX(x);
          if(d.bfs === 0 || typeof d.bfs === 'undefined')
            cx = middle.x;
          return cx;
        });

      setLinkEnds();
      setViewbox();
      setLabels();
    }
    function evaluateFallthrough(){
      falls += 1;
      if(fallthrough.length > 0 && falls < graph.nodes.length){
        var ft = fallthrough.slice(0);
        fallthrough = [];
        for (var i = ft.length - 1; i >= 0; i--) {
          evaluateDirection(ft[i][0],ft[i][1]);
          if(i === 0){
            evaluateFallthrough();         
          }
        };        
      }else{
        buildViz();
      }
    }
    function evaluateDirection(s,t){
        var sign = 0, number = 0;
        if(graph.nodes[nodeIDs[s]].hasOwnProperty("direction")){
          number = graph.nodes[nodeIDs[s]].direction;
          number > 0 ? sign = 1 : number == 0 ? sign = -1 : sign = -1;
          if(!("direction" in graph.nodes[nodeIDs[t]])){
            graph.nodes[nodeIDs[t]].direction = sign;
          }
        }else if(graph.nodes[nodeIDs[t]].hasOwnProperty("direction")){
          number = graph.nodes[nodeIDs[t]].direction;
          number > 0 ? sign = 1 : number == 0 ? sign = 1 : sign = -1;
          if(!("direction" in graph.nodes[nodeIDs[s]])){
            graph.nodes[nodeIDs[s]].direction = sign;
          }
        }else{
          fallthrough.push([s,t]);
        }
    }

  }
  function Graph() {
    var neighbors = this.neighbors = {}; // Key = vertex, value = array of neighbors.

    this.addEdge = function (u, v) {
      if (neighbors[u] === undefined) {  // Add the edge u -> v.
        neighbors[u] = [];
      }
      neighbors[u].push(v);
      if (neighbors[v] === undefined) {  // Also add the edge v -> u in order
        neighbors[v] = [];               // to implement an undirected graph.
      }                                  // For a directed graph, delete
      neighbors[v].push(u);              // these four lines.
    };

    return this;
  }
  function resetSelect(){
    d3.select("#cur").selectAll(".node").moveToFront(".nodes");
    d3.select("#cur").selectAll(".link").moveToFront(".links"); 
    d3.select("body").classed("mousedover", false);
    d3.selectAll(".node").classed("parent child active", false);
  }
  function sortChildDe(a, b) {
    return parseFloat(b.shortest) - parseFloat(a.shortest);
  }
  function sortChildAs(a, b) {
    return parseFloat(b.shortest) - parseFloat(a.shortest);
  }  
  function activeNode(d){
    d3.select("#node"+d.field_521).classed("active", true);
    d3.select("#node"+d.field_521).moveToFront("#cur .nodes");
    d3.select("#label"+d.field_521).moveToFront("#cur .labels");
    d3.select(".links").selectAll(".link")
      .classed("active", function(dd,i){
        var front = "from"+d.field_521+"to";
        var infront = d3.select(this).attr("id").indexOf(front);
        var back = "to"+d.field_521;
        var inback = d3.select(this).attr("id").endsWith(back);
        if(infront > -1 || inback){
          d3.select(this).moveToFront("#cur .links");
          return true;
        }else{
          return false;
        }
      });
    stats.selectAll("*").remove();
    stats.append("h3")
      .text(d.field_520);
    var p = stats.append("ul")
         .attr("id", "parents");
    var c = stats.append("ul")
         .attr("id", "children");
    p.append("h5").text("Equity Holders");
    c.append("h5").text("Subsidiaries / Investments");
  }
  function childrenDisplay(d){
    d3.select("body").classed("mousedover", true);
    for(var j = 0; j < d.children.length; j++){
      var c = d3.select("#node"+d.children[j].id);
      var l = d3.select("#label"+d.children[j].id);
      var o = d.children[j].percent;
      childrenTasks(c, "#cur .nodes",o);
      childrenTasks(l, "#cur .labels",o);
      listChildren(c,o);   
    }
  }
  function childrenTasks(c,s,o){
    c.moveToFront(s);
    showChildren(c); 
  }
  function showChildren(c){
    c.classed("child", true);
  }
  function listChildren(c,o){
    d3.select("#children").append("li")
      .text(c.datum()[nodeTitle] + " " + o);
  }
  function parentDisplay(d){
    d3.select("body").classed("mousedover", true);
    for(var j = 0; j < d.parents.length; j++){
      var c = d3.select("#node"+d.parents[j].id);
      var l = d3.select("#label"+d.parents[j].id);
      var o = d.parents[j].percent;
      parentTasks(c, "#cur .nodes", o);
      parentTasks(l, "#cur .labels", o);
      listParents(c,o); 
    }
  }
  function parentTasks(c,s,o){
    c.moveToFront(s);
    showParents(c);   
  }
  function showParents(p){
    p.classed("parent", true);
  }
  function listParents(p,o){
    d3.select("#parents").append("li")
      .text(p.datum()[nodeTitle] + " " + o);
  }  
  function bfs(graph, source) {
    var queue = [ { vertex: source, count: 0 } ],
        visited = { source: true },
        tail = 0;

    while (tail < queue.length) {
      var u = queue[tail].vertex,
          count = queue[tail++].count;  // Pop a vertex off the queue.

      //we never want to eveluate the source node path as anything except 0
      if(source === u)
          count = 0;

      d3.select("#node"+u)
        .attr("y", function(d,i){
          d.bfs = count;
          d.level = d.bfs;
          var cy = circleY(d.bfs,d.direction);
          if(d.bfs === 0 || typeof d.bfs === 'undefined')
            cy = middle.y;
          return cy;
        })
        .classed("connected depth"+count, true);

      d3.select("#label"+u)
        .classed("connected", true);

      graph.neighbors[u].forEach(function (v) {
        if (!visited[v]) {
          visited[v] = true;
          queue.push({ vertex: v, count: count + 1 });
        }
      });
    }
  }
  function setLabels(){
    var labels = d3.select(".labels").selectAll(".label")
      .attr("x", function(d,i){
        return d3.select("#node"+d[nodeSource]).attr("x");
      })
      .attr("y", function(d,i){
        return d3.select("#node"+d[nodeSource]).attr("y");        
      });

    labels.append("tspan")
          .attr("x", function(d,i){
            return parseFloat(d3.select("#node"+d[nodeSource]).attr("x")) + radius.w;
          })
          .attr("y", function(d,i){
            return d3.select("#node"+d[nodeSource]).attr("y");        
          })
          .attr("dy", "5px")
          .text(function(d){
            return d[nodeTitle].substring(0, 20);
          });    
    labels.append("tspan")
      .attr("x", function(d,i){
        return parseFloat(d3.select("#node"+d[nodeSource]).attr("x")) + radius.w;
      })
      .attr("y", function(d,i){
        return d3.select("#node"+d[nodeSource]).attr("y");        
      })
      .attr("dy", "10px")
      .text(function(d){
        return d[nodeTitle].substring(20, 40);
      });        
  }
  function circleY(count,dir){
    return -(count * space * radius.h * dir);
  }
  function circleX(n){
    var op = 1;
    var offset = 0;
    if(n !== 1){
      if(n % 2 === 0){
        op = -1;
      }
      offset = op * xPos(n);
    }
    return middle.x + offset;    
  }
  function xPos(n){
    var pos = Math.floor(n / 2) * (space * radius.w);
    return pos;
  }
  function hasChildY(t,d){
    //set initial cy to start
    var cy = d3.select(t).attr("y");
    var count = 0;
    var dir = d.direction;
    //sort the children so the oldest is first
    var sf = sortChildDe;
    d.children.sort(sf);

    //decided if we are working with an upper or lower oldest child
    if(d.children[0].shortest > 0){
      //if the oldest child is upper
      //move up
      dir = 1;
      count = Math.abs(d.children[0].shortest) + dir;
    }else{
      //if the oldest child is lower
      // DO NOTHING
      if(d.direction < 0){
        //if it is a lower node
        count = Math.abs(d.children[0].shortest) + dir;
      }
    }

    if(count !== 0){
      d.level = count;
      d.direction = dir;
      cy = circleY(count, dir);
    }

    return cy;
  }  
  //this sets a layout for the hierarchy model used for layout
  function setHierarchy(c,l){
    var d = c + 1;
    if(d > hierarchy[l].length){
      for (var i = hierarchy[l].length; i < d; i++) {
        hierarchy[l].push(0);
      };
    }
    hierarchy[l][c] += 1;
    return hierarchy[l][c];
  }
  function shortestPath(graph, source, target) {
    if (source == target) {   // Delete these four lines if
      print(source);          // you want to look for a cycle
      return;                 // when the source is equal to
    }                         // the target.
    var queue = [ source ],
        visited = { source: true },
        predecessor = {},
        tail = 0;
    while (tail < queue.length) {
      var u = queue[tail++],  // Pop a vertex off the queue.
          neighbors = graph.neighbors[u];
      for (var i = 0; i < neighbors.length; ++i) {
        var v = neighbors[i];
        if (visited[v]) {
          continue;
        }
        visited[v] = true;
        if (v === target) {   // Check if the path is complete.
          var path = [ v ];   // If so, backtrack through the path.
          while (u !== source) {
            path.push(u);
            u = predecessor[u];
          }
          path.push(u);
          path.reverse();
          print(path.join(' &rarr; '));
          return;
        }
        predecessor[v] = u;
        queue.push(v);
      }
    }
    print('there is no path from ' + source + ' to ' + target);
  }
  function buildElbows(d){
    var source = d3.select("#node"+d.source);
    var target = d3.select("#node"+d.target);
    var sourceX = parseFloat(source.attr("x"));
    var targetX = parseFloat(target.attr("x"));
    var sourceLevel = source.data()[0].level * source.data()[0].direction;
    var targetLevel = target.data()[0].level * target.data()[0].direction;
    var levelDiff = Math.abs(sourceLevel - targetLevel);
    var xDiff = sourceX - targetX;
    var x1 = sourceX + radius.w;
    var y1 = (parseFloat(source.attr("y")) + radius.h);
    var x3 = targetX + radius.w;
    var y3 = (parseFloat(target.attr("y")) + radius.h);
    var y2 = y1 - (radius.h * (space / 2));
    var x2 = x3 - (radius.w * (space / 2));
    if(y1 < y3){
      y2 = y1 + (radius.h * (space / 2));
    }
    if(x1 > x3){
      x2 = x3 + (radius.w * (space / 2));
    }
    //if the source and the target are 1 level apart
    //and are in the same column
    if(levelDiff === 1 && xDiff === 0){
      x2 = x1;
      y2 = y1;
    }
    var p1 = [x1,y1];
    var p2 = [x1,y2];
    var p3 = [x2,y2];
    var p4 = [x2,y3];
    var p5 = [x3,y3];
    return p1 + " " + p2 + " " + p3 + " " + p4 + " " + p5;    
  }
  function setLinkEnds(t){
    var time = t || 0;
    d3.select('.links').selectAll('.link')
        .transition().duration(time)
        .attr("points", function(d){
          return buildElbows(d);
        });  
  }
  function setViewbox(){
    var maxHorizon = d3.max([d3.max(hierarchy.upper), d3.max(hierarchy.lower)]);
    var xp = xPos(maxHorizon);
    var ogw = width;
    var ogh = height;
    var x = middle.x - xp - (radius.w * 2);
    width = (xp * 2) + (radius.w * 4);
    height = (hierarchy.upper.length + hierarchy.lower.length + 1) * 4 * radius.h;
    baseYTransform = hierarchy.upper.length * 4 * radius.h;

    elm.attr("transform", "translate(0," + baseYTransform + ")");
    svg.attr("viewBox", x+" 0 "+width+" "+height);

    zoom.scaleExtent([1, d3.max([(height/ogh),(width/ogw)]) * 2])
      .translateExtent([[x, -height/2], [width, height]]);

    //only set zoom if zoom is needed    
    if((width*3) > window.innerWidth || (height*3) > window.innerHeight){
      console.log("zoom enabled");
      svg.call(zoom);
    }
  }
  function loadedSort(arr){
    var arrClone = arr.slice(0);
    arr = [];
    for (var i = 0; i < arrClone.length; i++) {
      if (arrClone[i][linkSource] === centralID) {
        arr.push(arrClone[i]);
      }
    }
    for (var i = 0; i < arrClone.length; i++) {
      if (arrClone[i][linkTarget] === centralID) {
        arr.push(arrClone[i]);
      }
    }
    for (var i = 0; i < arrClone.length; i++) {
      if (arrClone[i][linkTarget] !== centralID && arrClone[i][linkSource] !== centralID) {
        arr.push(arrClone[i]);
      }
    }
    return arr;
  }
  function zoomed() {
    var transform = d3.event.transform;
    var scale = transform.k;
    var x = transform.x;
    var y = transform.y;
    if(scale === 1){
      x = 0;
      y = baseYTransform;
    }
    d3.select("#elements")
      .attr("transform", "translate(" + x + "," + y + ") scale("+ scale +")");
  }

  function resetted() {
    svg.transition()
        .duration(750)
        .call(zoom.transform, d3.zoomIdentity);
  }
  d3.selection.prototype.moveToFront = function(el) {  
    return this.each(function(){
      d3.select(el).node().appendChild(this);
    });
  };
  if (typeof String.prototype.endsWith !== 'function') {
      String.prototype.endsWith = function(suffix) {
          return this.indexOf(suffix, this.length - suffix.length) !== -1;
      };
  }   
}